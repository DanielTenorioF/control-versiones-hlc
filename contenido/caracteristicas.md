# Caracteristicas


**Distribuido**: Git es un sistema distribuido, lo que significa que cada desarrollador tiene una copia completa del historial de cambios en su máquina local. Esto facilita el trabajo en entornos descentralizados y permite a los desarrolladores trabajar sin conexión a Internet.

**Historial de cambios**: Git mantiene un historial detallado de todos los cambios realizados en un proyecto, lo que facilita la revisión y el seguimiento del progreso a lo largo del tiempo.

**Ramificación**: Una característica clave de Git es la capacidad de crear ramas. Las ramas permiten a los desarrolladores trabajar en nuevas características o solucionar problemas sin afectar la rama principal (normalmente llamada "master" o "main"). Luego, estas ramas se pueden fusionar de nuevo en la rama principal.

**Rápido y eficiente**: Git es conocido por su velocidad y eficiencia en la gestión de grandes proyectos y grandes conjuntos de datos.

**Integridad de los datos**: Los datos en Git se almacenan de manera segura mediante un sistema de hash criptográfico. Cada cambio, cada archivo y cada directorio tiene su identificador único, lo que garantiza la integridad del sistema.

![image](/img/caracteristicas.png)